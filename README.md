# Timeline:
[TOC]
## 2023-08-14:

### Concatenated all .fastq.gz files from fastq_pass into a single .fastq.gz file.

Script:
```
cat /archive/projects/PromethION/OrgOne/xferFpenn/unzip/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M/20230104_1952_1F_PAM24667_be40608b/fastq_pass/*.fastq.gz > 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fastq.gz
```

### Converted .fastq.gz to .fasta.

Script:
```
module load seqtk

seqtk seq -a 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fastq.gz > 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fasta
```

### Ran centrifuge/1.0.4-beta on .fasta file.

Script:
```
hostname
date

module load centrifuge/1.0.4-beta

# Run the centrifuge program with the following parameters:
# -f: specify the input file format as FASTA
# -x: specify the centrifuge index database to use
# --report-file: specify the filename of the output report
# --quiet: suppress non-error messages
# --min-hitlen: specify the minimum length of a hit to be considered
# -U: specify the input file (FASTA)
centrifuge -f \
        -x /core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v/abv \
        --report-file report.tsv \
        --quiet \
        --min-hitlen 50 \
        --threads 10 \
        -U /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/01_raw_reads/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fasta
```

### Ran kmerfreq on unfiltered .fasta file.

Script:
```
# calculate k-mer frequency using kmerfreq
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

echo "complete calculated k-mer frequency using kmerfreq"
```

Kmer individual number: `51865645146`

### Ran NanoPlot/1.33.0 on unfiltered .fastq.gz and .fasta file.

Script:
```
echo "HOSTNAME: `hostname`"
echo "Start Time: `date`"


module load NanoPlot/1.33.0

NanoPlot --fastq /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/01_raw_reads/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fastq.gz --loglength --verbose -o summary-fastqpass -t 10 -p summary-fastqpass


# Generate a summary plot of the raw reads with log-transformed data
NanoPlot --fasta /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/01_raw_reads/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fastq.gz --loglength --verbose -o summary-fasta -t 10 -p summary-fasta


echo "End Time: `date`"
date
module list
```

Read lengths vs. Average read quality dot graph:

![nanoplotbefore](/Figures/nanoplot_beforefilter.png)

Unfiltered NanoPlot data:
| NanoPlot | Raw Reads |
|---|---|
| Mean read length  | 13,193.4  |
|  Mean read quality |    16.6|
|Median read length   | 10,273.0   |
|   Median read quality| 17.0  |
| Number of reads  |    3,935,964.0 |
|   Read length N50| 21,550.0  |
| STDEV read length  |   11,332.2 |
|Total bases |   51,928,620,557.0|

## 2023-08-15:

### Ran GCE in homozygous and heterozygous mode on unfiltered .fasta file.

Script:
```
# extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum 

# run GCE in homozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 51865645146 -f read_files.lib.kmer.freq.stat.2colum >gce.table 2>gce.log

# run GCE in heterozygous mode 
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 51865645146 -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log
```

Estimated unfiltered genome size:

| Homozygous | Heterozygous   |
|---|---|
|1.70722G  | 1.70722G |

Estimated coverage: `26.8464x`

### Created coverage graph using RStudio.

Script:
```
library(readxl)

# Load the data from the Excel file (Please replace the path with the actual path to your Excel file)
Juglans_Coverage_graph <- read_excel("/Users/antsi/Downloads/read_files.lib.kmer.freq.stat.2colum.xlsx")

# View the loaded data
View(Juglans_Coverage_graph)

# Set the default width and height for higher resolution plots
width_inches <- 8  # Adjust this value as needed
height_inches <- 6  # Adjust this value as needed

# Set the PDF options
pdf.options(width = width_inches, height = height_inches)

# Your plot code using Juglans_Coverage_graph
plot(Juglans_Coverage_graph[15:250,], type = "l", xlab = "Kmer Count", ylab = "Kmer Frequency", main = "Kmer Frequency vs. Count Graph For Juglans ailantifolia", col = "palegreen3", lwd = 5.0)
```

Graph:

![kmerfreqbeforefilter](/Figures/kmerfreqvscntgrph_beforefilter.png)

### Removed contaminants using grep and awk.

Script:
```
echo "start"

module load seqkit
module load seqtk

#taking the classified reads from the STDOUT file and adding those to a new file 
grep -vw "unclassified" ../centrifuge_7068562.out > contaminated_reads.txt
#extracting just the IDs from the text file
awk NF=1 contaminated_reads.txt > contaminated_read_ids.txt
#keeping only unique IDs and removing any duplicates
sort -u contaminated_read_ids.txt > no_dup_centrifuge_contaminated_read_ids.txt

#adds only the sequences that DO NOT match the headers in the file we provided
seqkit grep -v -f no_dup_centrifuge_contaminated_read_ids.txt /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/01_raw_reads/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M.fastq.gz > 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq

#gzip/convert to fasta
gzip -c 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq > 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq.gz
seqtk seq -a 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq.gz > 2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fasta
```

### Created pavian graph of contaminants.

Script:
```
hostname
date

module load centrifuge/1.0.4-beta
index=/core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v
centrifuge-kreport -x $index/abv ../report.tsv > pavian_report.txt
```

Graph:

![pavianreport](/Figures/pavianreport.png)

### Ran kmerfreq on filtered .fasta file.

Script:
```
# calculate k-mer frequency using kmerfreq
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

echo "complete calculated k-mer frequency using kmerfreq"

# extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum
```

Kmer individual number: `50582021089`

### Ran NanoPlot/1.33.0 on filtered .fastq.gz and .fasta file.

Script:
```
echo "HOSTNAME: `hostname`"
echo "Start Time: `date`"


module load NanoPlot/1.33.0

NanoPlot --fastq /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/02_quality_control/Centrifuge/Filter/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq.gz --loglength --verbose -o summary-fastqpass -t 10 -p summary-fastqpass

# Generate a summary plot of the raw reads with log-transformed data
NanoPlot --fasta /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/02_quality_control/Centrifuge/Filter/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq.gz --loglength --verbose -o summary-fasta -t 10 -p summary-fasta


echo "End Time: `date`"
date
module list
```

Graph:

## 2023-08-16

### Ran GCE in homozygous and heterozygous mode on filtered .fasta file.

Script:
```
# run GCE in homozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 51865645146 -f read_files.lib.kmer.freq.stat.2colum >gce.table 2>gce.log

# run GCE in heterozygous mode 
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g 51865645146 -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log
```

Estimated filtered genome size:

| Homozygous | Heterozygous   |
|---|---|
|1.712G  | 1.712G |

Estimated Coverage: `26.8381x`

### Created coverage graph using RStudio.

Script:
```
library(readxl)

# Load the data from the Excel file (Please replace the path with the actual path to your Excel file)
Juglans_Coverage_graph <- read_excel("/Users/antsi/Downloads/read_files.lib.kmer.freq.stat.2colum.xlsx")

# View the loaded data
View(Juglans_Coverage_graph)

# Set the default width and height for higher resolution plots
width_inches <- 8  # Adjust this value as needed
height_inches <- 6  # Adjust this value as needed

# Set the PDF options
pdf.options(width = width_inches, height = height_inches)

# Your plot code using Juglans_Coverage_graph
plot(Juglans_Coverage_graph[15:250,], type = "l", xlab = "Kmer Count", ylab = "Kmer Frequency", main = "Kmer Frequency vs. Count Graph For Juglans ailantifolia", col = "palegreen3", lwd = 5.0)
```

Graph:

![kmerfreqafterfilter](/Figures/kmerfreqvscntgrph_afterfilter.png)

### Ran flye/2.9.1 on filtered .fastq.gz file.

Script:
```
#!/bin/bash
#SBATCH --job-name=flye
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=300G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anthony.he@uconn.edu
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err


module load flye/2.9.1

flye --nano-hq /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/02_quality_control/Centrifuge/Filter/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fastq.gz --no-alt-contigs --threads 32 --out-dir flye-output --scaffold
```

### Ran canu/2.2 on filtered .fasta file

Script: 
```
#!/bin/bash
#SBATCH --job-name=canu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G 
#SBATCH --mail-user=anthony.he@uconn.edu
#SBATCH -o canu%x_%j.out
#SBATCH -e canu%x_%j.err

module load gnuplot/5.2.2
module load canu/2.2

canu -p canu -d canu_assembly genomeSize=1.712G \
        -minReadLength=5000 \
        -gridOptions="--partition=general --qos=general" canuIteration=1 \
        -nanopore /core/projects/EBP/conservation/japanese_walnut/Student_Japanese_walnut_dir/assembly/00_Git_Lab_information/anthony-git/FpennTest/02_quality_control/Centrifuge/Filter/2023JAN04_OrgOne_Fpennsylvanica_grn_V14M_filtered.fasta
```
